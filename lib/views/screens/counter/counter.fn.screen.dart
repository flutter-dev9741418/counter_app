import 'package:counter_app/views/widgets/floatbuttom.widget.dart';
import 'package:flutter/material.dart';

class CounterFnScreen extends StatefulWidget {
  const CounterFnScreen({super.key});

  @override
  State<CounterFnScreen> createState() => _CounterFnScreenState();
}

class _CounterFnScreenState extends State<CounterFnScreen> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Contador Function",
            style: TextStyle(color: Colors.white, fontSize: 26),
          ),
          backgroundColor: Colors.deepPurpleAccent,
          actions: [
            IconButton(
              onPressed: () {
                counter = 0;
                setState(() {});
              },
              icon: const Icon(
                Icons.refresh_rounded,
                color: Colors.white,
                size: 30,
              ),
            ),
          ],
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "$counter",
                style:
                    const TextStyle(fontSize: 160, fontWeight: FontWeight.w100),
              ),
              Text(counter != 1 ? 'Clicks' : 'Click',
                  style: const TextStyle(fontSize: 25))
            ],
          ),
        ),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            CustomFloatButtom(
              icon: Icons.plus_one,
              onClick: () {
                counter++;
                setState(() {});
              },
            ),
            const SizedBox(
              height: 15,
            ),
            CustomFloatButtom(
              icon: Icons.exposure_minus_1_outlined,
              onClick: () {
                if (counter == 0) return;
                counter--;
                setState(() {});
              },
            ),
          ],
        ));
  }
}

import 'package:flutter/material.dart';

class CustomFloatButtom extends StatelessWidget {
  final IconData icon;
  final VoidCallback? onClick;

  const CustomFloatButtom(
      {super.key, required this.icon, required this.onClick});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      enableFeedback: true,
      elevation: 0,
      backgroundColor: Colors.deepPurpleAccent,
      onPressed: onClick,
      child: Icon(
        icon,
        color: Colors.white,
        size: 26,
      ),
    );
  }
}

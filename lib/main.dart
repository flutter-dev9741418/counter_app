import 'package:counter_app/views/screens/counter/counter.fn.screen.dart';
// import 'package:counter_app/views/screens/counter/counter.screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            useMaterial3: true, colorSchemeSeed: Colors.deepPurpleAccent),
        home: const CounterFnScreen());
  }
}
